/*
pul.cpp:
pul.hで定義したメンバ関数を実装するファイル.
 */

#include <cmath>
#include "pul.h"


static const double kappa = 0.6321; // \omega_X - E_b
static const double tf = 92562;//2.0661*pow(10,4); //final time
static const double dt = 5.0; //time step
//const comp R(-0.96553, 0.753493);

double aa, ba, ca, da;
double k, theta, t;

action::action(double p, double q, double r, double s)
{
  aa = p;
  ba = q;
  ca = r;
  da = s;
}

//construct envelope function
double pul::f(double a, double b, double c, double t)
{
  return a*exp(-pow(t-b,2)/(2*c*c));
}

//construct carrier function
double pul::g(double d, double t)
{
  //double d = 0.1932; //the frequency of the carrier wave (7983.9THz)
  return cos(2*M_PI*d*t);
}

//construct pulse
double pul::pulse(double a, double b, double c, double d, double t)
{
  //construct FEL function from envelope and carrier  
  return f(a, b, c, t)*g(d, t);
}

//construct vector potential
double action::vectorp(double t)
{
  /*double A = 0.0;
  while(t < tf){
    A += action::pulse(aa, ba, ca, da, t) * dt;
    t += dt;
    }*/
  double c = 20165.2;
  double d = 9.068*pow(10, -3);
  double u = 1/(2*c*c);
  double v = 2*M_PI*d;
  double A;
  //A = (1.0/v + (u*(-4.0*u*t*t + 2.0)/(v*v*v)))*exp(-u*t*t)*sin(v*t) - (2.0*u*t/(v*v))*exp(-u*t*t)*cos(v*t);
  A = 0.0;
  return A;
}

//calculate action integration
double action::func(double k, double theta, double t) //use t = initial time ti
{
  double F = -(0.50*pow(k,2) + kappa) * t;
  //時刻tiから時刻\infty(現実的には無理なので, pulseが終わる時)までの作用積分の実行
  /*while(t < tf){
    //F += dt * 0.50 * (pow(k,2) - 2 * k * vectorp(t) * cos(theta) + pow(vectorp(t),2));
    F += dt * 0.50 * pow(k,2);
    t += dt;
    }*/
  
  return F;
}

//calculate theta0 which is an angular when photoelectron is emitted.
double action::theta0(double k, double theta, double t)
{
  double x;
  //x = k*sin(theta)/(k*cos(theta) - vectorp(t));//A_Lのクラス入れる必要ありか？？
  x = k*sin(theta)/(k*cos(theta));
  return atan(x);
 
  //return 0.0;
}

//calculate angular part of partial wave
double action::Y2(double ang)
{
  return 5.0*(3*pow(cos(ang), 2)-1.0)/3.0;
  //return 10.0/3.0; 
}

//calculate dipole element
/*comp action::dipole(double k, double theta, double t) 
{
  return 1-R*Y2(theta0(k, theta, t));    
}
*/
