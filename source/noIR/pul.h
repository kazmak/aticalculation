/*
pul.h:パルスを再現するためのクラスの定義と実装.
envelope はGaussian, carrierはsinと仮定.
作用積分・SAEでの双極子モーメントあたりのクラスの定義
なお, SAEでの双極子モーメントが一電子のモーメントに近似できることを用いると
Y^0_0 - RY^0_2(\theta_0, t)
となる.
 */

//パルスクラスの定義
class pul
{
  double f(double a, double b, double c, double t); //envelope functuion
  double g(double d, double t); //carrier function
 public:
  double pulse(double a, double b, double c, double d, double t); //pulse function
  //pul(double a, double b, double c, double d, double t);
};

//作用積分のクラス定義
class action : public pul
{
  //double vectorp(double t);
 public:
  double theta0(double k, double theta, double t);
  double Y2(double ang);
  action(double p, double q, double r, double s);
  //comp dipole(double k, double theta, double t);
  double func(double k, double theta, double t);
  double vectorp(double t);
};
