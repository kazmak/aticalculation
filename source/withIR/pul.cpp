/*
pul.cpp:
pul.hで定義したメンバ関数を実装するファイル.
 */

#include <cmath>
#include "pul.h"

static const double kappa = 0.6321; // \omega_X - E_b
static const double coeff = 0.05398*0.81/sqrt(100.0);;

double aa, ba, ca, da;
double k, theta, t;

action::action(double p, double q, double r, double s)
{
  aa = p;
  ba = q;
  ca = r;
  da = s;
}

//construct envelope function
double pul::f(double a, double b, double c, double t)
{
  return a*exp(-pow(t-b,2)/(2*c*c));
}

//construct carrier function
double pul::g(double d, double t)
{
  return cos(2*M_PI*d*t);
}

//construct pulse
double pul::pulse(double a, double b, double c, double d, double t)
{
  //construct FEL function from envelope and carrier  
  return f(a, b, c, t)*g(d, t);
}

//construct vector potential
double action::vectorp(double t)
{
  double c = 20165.2;
  double d = 9.068*pow(10, -3);
  double u = 1/(2*c*c);
  double v = 2*M_PI*d;
  double A;
  A = (1.0/v + (u*(-4.0*u*t*t + 2.0)/(v*v*v)))*exp(-u*t*t)*sin(v*t) - (2.0*u*t/(v*v))*exp(-u*t*t)*cos(v*t);
  double b = -coeff*A;
  return b;
}

//calculate action integration(ただし, A^2についてはmainで別に計算.)
double action::func(double k, double theta, double t)
{
  double c = 20165.2;
  double d = 9.068*pow(10, -3);
  double p = 1/(2*c*c);
  double q = 2*M_PI*d;
  double F = -(0.50*pow(k,2) + kappa) * t;
  double x = ((1.0/pow(q,2) + 6.0*p*(-2.0*p*t*t + 1.0)/pow(q,4))*cos(q*t) + (4.0*p*t/pow(q,3) + 6.0*p*(-6.0*p*t+4.0*p*p*pow(t,3))/pow(q,5))*sin(q*t))*exp(-p*t*t)*(-1);//integral of A.(-1)は間違い補正のため.
  F += -k*cos(theta)*x*coeff; //vector potentialの定数係数を追加.
  return F;
}


//calculate theta0 which is an angular when photoelectron is emitted.
double action::theta0(double k, double theta, double t)
{
  double x;
  x = k*sin(theta)/(k*cos(theta) - vectorp(t));
  return atan(x);
}

//calculate angular part of partial wave
double action::Y2(double ang)
{
  return 5.0*(3*pow(cos(ang), 2)-1.0)/3.0;
}
