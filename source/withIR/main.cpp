/*
main.cpp:Ar 3p軌道ATIの数値計算を行うメイン.単位はすべて原子単位系.
xfelのパルス幅を300fsに修正.
IRの強度変える時はaction IRの1つ目の変数を適宜書き換えるようにする.(ここでは1.01*10^{14}[W/cm^2])
 */

#include <iostream>
#include <fstream>
#include <cmath>
#include <complex>
#include "pul.h"

using namespace std;
typedef std::complex<double> comp;

const comp RUINT(1.0,0.0);
const comp IUNIT(0.0,1.0);

main(){

  double k; //momentum
  double theta; //angular(final state)
  double t;
  comp Amp;
  pul XFEL;
  action IR(0.05398, 0.0, 20165.2, 9.068*pow(10, -3));
  double fel;
  double tstep = 0.6;
  double energy = 5.0/13.6;
  comp R(-0.96553, 0.753493);
  double f;
  
  ofstream output("A.dat");//open output data file

//calculate integral of A^2
  double q;
  double S = 0.0;
  static const double tf = 100000;
  while(t <= tf){
    q = IR.vectorp(t);
  S += q*q*tstep;
  t += tstep;
  }

//calculate amplitude and intensity
  while(energy <= 30.01/13.6){
    k = sqrt(energy);
    theta = 0.0;
    while(theta < M_PI*91.0/180.0){
      Amp = 0.0;
      t = -0.6*1.0330*pow(10,4);
      f=0.5*S;//integral of A^2
      fel = XFEL.pulse(1.0, 0.0, 4132.2*3.0, 0.1932, t);//the width of the pulse is 100fs, and the frequency of the carrier wave (7983.9THz)
      Amp += tstep*fel*(1.0-R*IR.Y2(IR.theta0(k, theta, t)))*exp(IUNIT*(IR.func(k, theta, t)+f));
      t += tstep;
      while(t <= 0.60*1.0330*pow(10,4)){
	fel = XFEL.pulse(1.0, 0.0, 4132.2*3.0, 0.1932, t);//the width of the pulse is 100fs, and the frequency of the carrier wave (7983.9THz)
	f -= 0.5*IR.vectorp(t-tstep)*IR.vectorp(t-tstep)*tstep;
	Amp += tstep*fel*(1.0-R*IR.Y2(IR.theta0(k, theta, t)))*exp(IUNIT*(IR.func(k, theta, t)+f));
	t += tstep; //add time step
      }
      output << 33.0 - energy*13.6 << " " << theta << " " << abs(Amp)*abs(Amp) << "\n";
      theta += M_PI/180.0;
    }    
    energy += 0.01/13.6;
  }

  output.close();
  return 0;
}
