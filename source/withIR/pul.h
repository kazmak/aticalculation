/*
pul.h:パルスを再現するためのクラスの定義と実装.
envelope はGaussian, carrierはcosと仮定.
作用積分・SAEでの双極子モーメントあたりのクラスの定義
 */

//パルスクラスの定義
class pul
{
  double f(double a, double b, double c, double t); //envelope functuion
  double g(double d, double t); //carrier function
 public:
  double pulse(double a, double b, double c, double d, double t); //pulse function
};

//作用積分のクラス定義
class action
{
 public:
  action(double p, double q, double r, double s);
  double theta0(double k, double theta, double t);
  double Y2(double ang);
  double func(double k, double theta, double t);
  double vectorp(double t);
};
