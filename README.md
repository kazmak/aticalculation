#readme.md
フォルダの内容を簡単にまとめておきました.ご確認お願いいたします.

##calculation.pdf
どのような計算をしてどういう結果が得られたかを簡単にまとめたもの.

##A0
IR laserがないときの計算結果.

* theta0.eps:

photoelectronの運動量ベクトルとvector potentialのなす角thetaが0のときの結果.
ここでは33-I_p=17.2(eV)のところにピークが出ている.

* result.eps:

photoelectronのエネルギーとthetaをパラメータとしたときの強度を3Dマッピングした図.

* polar.eps:

数値計算で得た結果を実験結果と似たような形式にした図.

##A1
IR laserを実験値(ビーム径100\mu m, エネルギー4mJ, IRのパルス幅488fs, 強度は1.01*10^{14}W/cm^2)に基づいて計算した時の結果.ファイル名はA0に準ずる.

##A2
IRの強度を変えて計算し, A0でのピークとそのside bandが同じくらいの強度になったときのIRの強度(6.5*10^{11} w/cm^2)にして計算した時の結果.ファイル名はA0に準ずる.

* angular.eps:

main peakとその前後のside bandピークでの角度分布をプロットしたもの. 

##source
今回の数値計算で用いたソースコード.
* noIR

IRを入れない場合の計算

* with IR

IRを入れた場合の計算.

##transition.eps
数値計算のイメージ図.

##実験結果.pdf
実験結果のデータを写真でスキャンしたもの.
